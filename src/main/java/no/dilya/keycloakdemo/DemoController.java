package no.dilya.keycloakdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class DemoController {

    @GetMapping(value = "/demo")
    public SenderResponse adminEndpoint() {
        return new SenderResponse("Hello From Admin");
    }

    @GetMapping(value = "/manager")
    public SenderResponse managerEndpoint() {
        return new SenderResponse("Hello From Manager");
    }
}
